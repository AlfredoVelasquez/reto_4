def sql
  <<-SQL
    select
      b.id as identificacion,
      b.title as titulo,
      b.author as autor,
      sum(d.quantity) as cantidad
    from books as b
    inner join details d on d.book_id = b.id
    group by 1
    order by 4 DESC
  SQL
end